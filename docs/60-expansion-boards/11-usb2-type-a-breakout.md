---
layout: default3
title: USB2 Type A Breakout
nav_order: 11
parent: Expansion Boards
has_children: true
thumbnail: /other-products/m0141/m0141-top-thumb.png
permalink: /usb2-type-a-breakout-add-on/
summary: This Expansion Adapter integrates USB 2 peripheral devices as well as an additional UART, I2C and GPIO for VOXL 2
---

# USB2 Type A Breakout (M0141)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The M0141 USB2 Type A Breakout board provides:

- a USB2 Type A Female connection
- an apps proc UART, I2C and GPIOs (VOXL 2 Only)
- momentary fastboot switch
 
<img src="/images/other-products/m0141/m0141-top.jpg" alt="m0141-top" width="320"/>

<img src="/images/other-products/m0141/m0141-bottom.jpg" alt="m0141-bottom" width="320"/>


## Mechanical Drawing and Spec

[Download STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0141%20USB2%20Type%20A%20Breakout.step)


| Feature      | Details                    |
|--------------|----------------------------|
| Weight       | 6.3g                       |


## Revisions

| PN             | Information                                                  |
|----------------|--------------------------------------------------------------|
| MDK-M0141-1-00 | TODO |


## Schematic

[Download Schematic](https://storage.googleapis.com/modalai_public/modal_schematics/M0141_Schematic_REVA.pdf)

## Connector Callouts

<img src="/images/other-products/m0141/m0141-callouts-1.jpg" alt="m0141-callouts" width="640"/>

### J2 - USB2

- Board Connector - ``
- Mating Connector - ``

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5P0V                 |                           |
| 2    | USB_N                |                           |
| 3    | USB_P                |                           |
| 4    | GND                  |                           |

### J5 - UART/I2C/GPIO

Supported on VOXL 2 only.

- Board Connector - `SM12B-GHS-TB(LF)(SN)`
- Mating Connector - `GHR-12V-S`

Signals are 3P3V, see [Linux User Guide](/voxl2-linux-user-guide/) for details on interfaces.

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 3P3V                 |                           |
| 2    | GPIO_52 (in)         | alt SPI17 MISO            |
| 3    | GPIO_53 (out)        | alt SPI17 MOSI            |
| 4    | GPIO_54 (out)        | alt SPI17 SCLK            |
| 5    | GPIO_55 (out)        | alt SPI17 CS_N            |
| 6    | GPIO_32 (in/out)     |                           |
| 7    | GPIO_33 (in/out)     |                           |
| 8    | I2C9_SDA             | `/dev/i2c-1`              | 
| 9    | I2C9_SCL             | `/dev/i2c-1`              |
| 10   | UART7_TX             | `/dev/ttyHS1`             |
| 11   | UART7_RX             | `/dev/ttyHS1`             |
| 12   | GND                  |                           |

### SW1

Force Fastboot momentary button.
