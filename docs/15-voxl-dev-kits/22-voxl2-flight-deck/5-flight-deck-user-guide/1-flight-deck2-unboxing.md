---
layout: default
title: VOXL 2 Flight Deck Unboxing
parent: VOXL 2 Flight Deck User Guide
nav_order: 1
has_children: false
permalink: /voxl2-flight-deck-userguide-unboxing/
---

# VOXL 2 Flight Deck Unboxing 

<img src="/images/voxl2-flight-deck/sentinel-flight-deck.png" alt="sentinel-flight-deck" width="60%">

---
## Required Materials:

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Wall Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 3300mAh, or any 4S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 
  - Buy Wall Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi

<hr>


## Whats in the Box:

### 5G LTE Modem

<img src="/images/voxl2-flight-deck/flight-deck-5g-lte.jpg" alt="voxl2-5g" width="60%">

### 2.4/5ghz WiFi 

<img src="/images/voxl2-flight-deck/flight-deck-wifi.jpg" alt="voxl2-wifi" width="60%">

### Microhard 

(Microhard Modem is sold Separately)

<img src="/images/voxl2-flight-deck/flight-deck-microhard.jpg" alt="voxl2-microhard" width="60%">

[Next: Connections](/voxl2-flight-deck-userguide-connections/){: .btn .btn-green }
