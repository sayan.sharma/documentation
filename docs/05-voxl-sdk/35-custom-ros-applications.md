---
title: Custom ROS Applications 
layout: default
parent: VOXL SDK
has_children: true
nav_order: 35
permalink: /ros/
---

# ROS Overview
{% include youtubePlayer.html id="wrqZFTiHyTU" %}

For developers with experience using ROS, we've built support for ROS into VOXL, as an alternative to coding within VOXL's [Modal Pipe Architecture](/mpa/) with `systemd` services and POSIX pipes. 

This approach is more of a wrapper around VOXL's Modal Pipe Architecture and introduces more overhead. The way it works is that a ROS node translates POSIX pipes in the VOXL Modal Pipe Architecture into topics that can be used in ROS.

High performance can be achieved either way.

