---
layout: default
title: Flight Core v2 USB to QGC
parent: Flight Core v2 User Guides
grand_parent: Flight Core v2
nav_order: 1
permalink: /flight-core-v2-usb-to-qgc/
---

# Flight Core v2 USB to QGC
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware

### J3 - USB / QGroundControl

To make a connection to QGroundControl (and power the Flight Core v2), use `J3` with the recommended is [MCBL-00010](/cable-datasheets/#mcbl-00010/) (JST-to-micro-USB) for this interface, when adding your own micro USB cable.

![Flight Core v2](/images/flight-core-v2/m0087-power-usb.png)

## QGC Software Notes

### All Host OS

QGC 4.3.0 and later supports firmware upgrade over USB. To update FW using older QGC, please see [work around](/flight-core-v2-firmware/).

### Windows Users

Please note, QGroundControl does **not** automatically make a connection to Flight Core v2 when running on Windows.

To make a connection:

- have FCv2 connected over USB
- go to **Application Settings** > **Comms Links**
- click on **Add**
- enter a name like "FCv2"
- set baud rate **115200** 
- click on the "Automatically Connect on Start" if you want
- confirm the "Serial Port" is correctly listed
- click **OK**

Select it from the list above, and click **Connect**.  
