---
layout: default
title: ELRS v0 Datasheet
parent: ELRS Radios
nav_order: 1
has_children: false
nav_exclude: true
search_exclude: true
permalink: /elrs-v0-datasheet/
---

# ELRS v0 Prototype Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

<!--<img src="/images/voxl2-io/user-guide/M0139-hero.png" width="240"/>-->

# Summary

## PROTOTYPE RELEASE

NOTE: THIS PRODUCT IS NOT INTENDED FOR PUBLIC USE AND IS A PROTOTYPE ONLY

Limitations:
- VOXL SDK 1.2+ is required
- only a single RF path is available the initial release using J1.

## Transmitter Compatibility

In general, follow [Express LRS guidance](https://www.expresslrs.org/hardware/hardware-selection/) selecting 900MHz options.

## VOXL SDK Support

SDK 1.2 or newer required.  For VOXL SDK usage, please see [voxl-elrs](/voxl-elrs/).

## Dimensions

### 3D Drawings

TODO
<!--[3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/TODOstep)-->

### 2D Drawings

TODO
<!-- <img src="/images/elrs-v1/m0139-2d.png"/> -->

## Features

| Feature          | Details                                                                                                                                                         |
|:-----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Weight           | TODO g                                                                                                                                                          |
| HxW              | 33mm x 22mm
| Frequency Bands  | 915Mhz                                                                                                                                                          |
| Input Voltage    | 5VDC                                                                                                                                                            |
| Serial Output    | CRSF, 420k default                                                                                                                                              |
| RF Chip          | Dual SX1276                                                                                                                                                     |
| Antenna          | IPEX MHF                                                                                                                                                        |
| Firmware         | [ELRS 3.2](https://gitlab.com/voxl-public/support/elrs-build-docker)                                                                                                                                  |
| MCU              | 72MHz, 32-bit ARM M3 [STM32F103C8T6](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html)                                                   |
| Memory           | 20Kb RAM                                                                                                                                                        |
|                  | 64Kbit Flash                                                                                                                                                    |
| Inputs           | UART, push button                                                                                                                                               |
| Outputs          | QTY PWM, UART, R/G LED                                                                                                                                          |

# Hardware

## Hardware Block Diagram

<img src="/images/expansion-boards/30-elrs-v0/M0139-block-diagram.png">

## Connectors

### Summary

*BETA release: prototype hardware shown here with bluewire*

<img src="/images/expansion-boards/30-elrs-v0/M0139-connector-callouts.png"/>

| Connector      | Description                             |
| ---            |  ---                                    |
| J1             | Antenna 1                               |
| J2             | Antenna 2 (not used in beta release)    |
| J4             | PWM, RC UART                            |
| S1             | Bind button                             |
| D1             | Green LED                               |
| D2             | Red LED                                 |

### J4 - Header Solder Pads

| Pin | Name     | Notes                                   |
| --- | ----     |  ---                                    |
| 1   | GND      |                                         |
| 2   | P0       | PWM Channel 0, 3P3V                     |
| 3   | P1       | PWM Channel 1, 3P3V                     |
| 4   | P2       | PWM Channel 2, 3P3V                     |
| 5   | P3       | PWM Channel 3, 3P3V                     |
| 6   | GND      |                                         |
| 7   | TX       | ELRS TX (to VOXL2), 3P3V, 420k CRSF     |
| 8   | RX       | ELRS RX (from VOXL2), 3P3V, 420k CRSF   |
| 9   | 5V       | Input Power, 5P0V                       |
| 10  | GND      |                                         |


### J1 - ANT1

Main RF path (915MHz).

### J2 - ANT2

NOT USED IN BETA RELEASE

### S1 - Bind button

Used to put receiver in bind mode.

### D1 - Green LED

Solid when bound.

### D2 - Red LED

Status LED, follows [ELRS standards](https://www.expresslrs.org/quick-start/led-status/?h=led#receiver-led-status)

## Recommended Antennas

IPEX MHF + T antenna.

## Programming Test Points

TODO

# Software

## Summary

| Component/Repo                                                                     | Notes                                    |
| ---                                                                                | ---                                      |
| [elrs-build-docker](https://gitlab.com/voxl-public/support/elrs-build-docker)      | Docker container that can clone/build FW with ease                               |
| [VOXL SDK](/voxl-sdk/)                                                             | Used to deploy all required software/firmware components to VOXL / ELRS hardware |
| [ExpressLRS_3.2.1](https://github.com/modalai/ExpressLRS_3.2.1/tree/M0139)         | Fork of Express LRS supporting ELRSv1 (goal is to mainline) that runs on ELRS v1 (M0139) |
| [crsf_rc](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/rc/crsf_rc) | PX4 driver running in voxl-px4 that communicates over CRSF to ELRS v1 hardware |
| [voxl-elrs](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs)           | VOXL2 command line utility, used to update FW/config on ELRS v1 from VOXL2, part of the SDK |
| FW bootloader                                                                      | Closed source, shared with [voxl2-io](/voxl2-io/) and [VOXL ESC](/voxl-escs/) |

## Software Block Diagram

<img src="/images/expansion-boards/30-elrs-v0/M0139-sw-block-diagram.png">

# User Guides

## VOXL SDK

For VOXL SDK usage, please see [voxl-elrs](/voxl-elrs/).

## VOXL 2 Wiring Guide (M0054)

<img src="/images/expansion-boards/30-elrs-v0/M0139-M0054-wiring-diagram.png">
