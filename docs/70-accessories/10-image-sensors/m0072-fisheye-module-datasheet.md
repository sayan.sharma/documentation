---
layout: default
title: M0072 Fisheye Module Datasheet
parent: Image Sensors
nav_order: 72
has_children: false
permalink: /M0072/
---

# VOXL Tracking Sensor Datasheet
{: .no_toc }

## MSU-M0072-1-01

### Specification

| Specification | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV7251-PB-v1.9-WEB.pdf) |
| Shutter        | Global                                                                               |
| Resolution     | 640x480                                                                              |
| Framerate      | 30,60,90Hz implemented on VOXL, sensor supports up to 120Hz                           |
| Data formats   | B&W 8 and 10-bit                                                                     |
| Lens Size      | 1/3.06"                                                                              |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 0.83mm                                                                               |
| F Number       | 2.0                                                                                  |
| Fov(DxHxV)     | 166° x 133° x 100°                                                                   |
| TV Distortion  | -20.77%                                                                              |

### Technical Drawings

#### 3D STEP File

#### 2D Diagram

[Download](/images/other-products/image-sensors/M0072-2D.jpg)

### Pin Out

## Samples of Tracking sensor on Sentinel.

### Indoor

![tracking_in.png](/images/other-products/image-sensors/Samples/tracking_in.png)
{:target="_blank"}

### Outdoor

![tracking_out.png](/images/other-products/image-sensors/Samples/tracking_out.png)
{:target="_blank"}
